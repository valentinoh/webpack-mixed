import amd from './src/amd';
import amdWithDojo from './src/amdWithDojo';
import commonjs from './src/commonjs';
import es6 from './src/es6';

amd();
new amdWithDojo();
commonjs();
es6();
