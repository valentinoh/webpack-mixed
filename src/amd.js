import showText from './util';

define([], function () {
  return function() {
    showText('I am happy to be imported from an AMD module!')
  };
});
