import showText from './util';

define([
  'dojo/_base/declare',
], declare => declare([], {
  constructor() {
    showText('I am happy to be imported from an AMD (with dojo declare) module!');
  }
}));

